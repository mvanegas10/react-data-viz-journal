npm run build

while getopts v: option
do
# shellcheck disable=SC2220
case "${option}"
in
v) version_tag=${OPTARG};;
esac
done

echo $version_tag
aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 435994096874.dkr.ecr.eu-central-1.amazonaws.com

docker build -t 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal:$version_tag .
docker tag 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal:$version_tag 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal:$version_tag
docker push 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal:$version_tag

kubectl delete -f deploy-conf.yml
kubectl apply -f deploy-conf.yml
