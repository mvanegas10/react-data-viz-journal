import React, { Fragment } from 'react'
import Auth from '../views/AuthStateApp.jsx'
import Home from '../views/Home.jsx'
import NavBar from '../components/NavBar.jsx'
import SegmentationAuvio from '../views/SegmentationAuvio.jsx'
import MySegments from '../views/MySegments.jsx'
import CRMSegmentsStats from '../views/CRMSegmentsStats.jsx'
import UpdateSegments from '../views/UpdateSegments.jsx'


var routes = [
    {
        path: '/login',
        name: 'Login',
        component: Auth,
        handler: () => <Fragment>
            <NavBar />
            <br/><br/>
            <Auth />
        </Fragment>
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        handler: () => (
            <Fragment>
                <NavBar />
                <br/><br/>
                <Home />
            </Fragment>
        )
    },
    {
        path: '/mySegments',
        name: 'MySegments',
        component: MySegments,
        handler: () => (
            <Fragment>
                <NavBar />
                <br/><br/>
                <MySegments />
            </Fragment>
        )
    },
    {
        path: '/segmentationAuvio',
        name: 'SegmentationAuvio',
        component: SegmentationAuvio,
        handler: () => (
            <Fragment>
                <NavBar />
                <br/><br/>
                <SegmentationAuvio />
            </Fragment>
        )
    },
    {
        path: '/crmStats',
        name: 'CRMSegmentsStats',
        component: CRMSegmentsStats,
        handler: () => (
            <Fragment>
                <NavBar />
                <br/><br/>
                <CRMSegmentsStats />
            </Fragment>
        )
    },
    {
        path: '/addSegment',
        name: 'UpdateSegments',
        component: UpdateSegments,
        handler: () => (
            <Fragment>
                <NavBar />
                <br/><br/>
                <UpdateSegments />
            </Fragment>
        )
    },
    {
        redirect: true,
        path: '/',
        to: '/home',
        name: 'Home'
    }
]

export default routes