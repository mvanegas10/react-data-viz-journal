const initialState = {
}

export default function reducer(state, action) {
    if (!state) {
        return initialState
    }
    switch (action.type) {
        case 'LOGIN': {
            return {
                ...state,
                ...action.payload
            }
        }
        case 'LOGOUT': {
            return initialState
        }        
        default:
            return state
    }
}