
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter, HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import thunk from 'redux-thunk';
import reducers from './reducers';
import routes from './routes/index';
require('dotenv').config()

const store = createStore(reducers, applyMiddleware(thunk));

ReactDOM.render(
	<Provider store={store}>
		<React.Fragment>
			<HashRouter>
			{/* <BrowserRouter basename='/journal/'> */}
				<Switch>
					{routes.map((prop, key) => {
						if (prop.redirect)
							return (
								<Redirect
									from={prop.path}
									to={prop.to}
									key={key}
								/>
							);
						return (
							<Route
								path={prop.path}
								render={prop.handler}
								key={key}
							/>
						);
					})}
				</Switch>
			</HashRouter>
			{/* </BrowserRouter> */}
		</React.Fragment>
	</Provider>,
	document.getElementById('root'),
);