import React from 'react';
import * as d3 from 'd3'
import Swatches from '../components/Swatches.jsx'
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { post } from '../actions/restAction'
import Author from '../components/Author.jsx';
import Loader from '../components/Loader.jsx';

function getQuarter(day) {
  return Math.floor(day.getMonth() / 3) + 1;
}

function getSizeScale(data) {
  let scale = d3.scaleLinear()
    .domain(d3.extent(data.nodes, d => d.n))
    .range([5, 20]);
  return scale
}

function getIdToNodeObj(data) {
  let idToNode = {}
  data.nodes.forEach(function (n) {
    idToNode[n.id] = n;
  })
  return idToNode
}

async function getStrokeWidthScale(data) {
  let strokeWidthScale = {}
  await data.nodes.map(node => {
    var widthStroke = d3.scalePow()
      .domain(d3.extent(data.links.filter(d => d.source === node.name), d => d.value))
      .range([1, 3])
    strokeWidthScale[node.name] = widthStroke
  })
  return strokeWidthScale
}

function getLastDayQuarter(day) {
  const quarter = getQuarter(day);
  let date = new Date(
    day.getFullYear() + Math.floor(3 * quarter / 12),
    (3 * quarter) % 12,
    1
  );
  return date.addDays(-1);
}

function drag(_) {
  return d3.drag()
    .on("start", _ => _)
    .on("drag", _ => _)
    .on("end", _ => _);
}

function hashCode(str) {
  var hash = 0,
    i,
    chr;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
}

function createSocioDemo(svgId, i, ds, title, totals, segment, data, color) {
  var margin = { top: 50, right: 15, bottom: 20, left: 15 };
  var w = (document.body.clientWidth * 0.9 / 3.5) > 250 ? 250 : (document.body.clientWidth * 0.9 / 3.5) - margin.left - margin.right;
  var height = 200 - margin.top - margin.bottom;

  var svg = d3.select(`#${svgId}`).html('')

  var ages = [15, 25, 35, 45, 55, 65, 75, 85]

  var ageScale = d3
    .scaleLinear()
    .domain(
      d3.extent(data, d => d.birth_deceny)
    )
    .range([20, ages.length * 16.5])

  var genderColor = d3.scaleOrdinal()
    .domain(color.text)
    .range(color.colors)

  svg
    .attr("width", w + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var data = ds.filter(
    d =>
      d.gender && d.birth_deceny && (segment ? d.segment === segment : 1 === 1)
  );
  var barHeight = height / ages.length;
  var users = d3.sum(data, d => d.users);

  var size = d3
    .scaleLinear()
    .domain(d3.extent(ds, d => d.users))
    .range([0, w / 2]);

  let groups = svg
    .selectAll('rect')
    .data(data)
    .enter();

  groups
    .append('rect')
    .attr(
      'x',
      d => margin.left + (d.gender === 'f' ? w / 2 - size(d.users) : w / 2)
    )
    .attr('y', d => ageScale(d.birth_deceny))
    .attr('width', d => size(d.users))
    .attr('height', barHeight)
    .attr('stroke', 'white')
    .attr('stroke-width', '2px')
    .style('fill', d => genderColor(d.gender))
    .append('title')
    .text(d => JSON.stringify(d));

  groups
    .append('rect')
    .attr('x', w / 2 + margin.left - 12.5)
    .attr('y', d => ageScale(d.birth_deceny) + 2.5)
    .attr('width', '25')
    .attr('height', '10')
    .attr('opacity', '0.5')
    .attr('fill', 'white');

  groups
    .append('text')
    .attr('x', w / 2 + margin.left)
    .attr('y', d => ageScale(d.birth_deceny) + barHeight / 2 + 2.5)
    .style("text-anchor", "middle")
    .style("font-size", "8px")
    .style("fill", "grey")
    .text(d => `${d.birth_deceny}-${d.birth_deceny + 10}`);

  if (i === 0) {
    svg
      .append("text")
      .attr('x', 0)
      .attr('y', 8)
      .style("text-anchor", "start")
      .style("font-size", "10px")
      .attr("font-weight", "bold")
      .text("Tranche d'âge");
  }

  svg
    .append('text')
    .attr('x', w / 2)
    .attr('y', 25)
    .style('text-anchor', 'middle')
    .style('font-family', `'Helvetica', sans-serif`)
    .html(
      `${title} <tspan style="font-size:10px">${d3.format(',')(
        users
      )} util. (${d3.format('.0%')(users / totals)})</tspan>`
    );

  return svg.node();
}


class Segments extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      width: document.body.clientWidth * 0.9,
      segments: ['fidèles', 'reconnus', 'occasionnels'],
      genderColor: {
        text: ['f', 'm'],
        colors: ['#8701f9', '#4ec4a9']
      },
      lastModified: undefined,
      fullNetData: undefined,
      currentQuarter: undefined,
      summaryData: undefined,
      percViews: undefined,
      fikelsColors: ["#8b1e3f", "#197BBD", "#89bd9e", "#f0c987", "#FE938C", '#9F7E69', '#fd8d3c'],
      segmentColors: {
        fideles: '#233571', fidèles: '#233571', reconnus: '#89aade', occasionnels: '#d9e0f4'
      },
      quarters: undefined
    }

    Date.prototype.addDays = function (days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
    }

    Date.prototype.addYears = function (years) {
      var d = new Date(this.valueOf());
      var year = d.getFullYear();
      var month = d.getMonth();
      var day = d.getDate();
      var c = new Date(year + years, month, day);
      return c;
    }

    this.getGroups = this.getGroups.bind(this)
    this.createHeatMap = this.createHeatMap.bind(this)
    this.createSegmentsDistribution = this.createSegmentsDistribution.bind(this)
    this.createNetwork = this.createNetwork.bind(this)
    this.createArcDiagram = this.createArcDiagram.bind(this)
    this.loadData = this.loadData.bind(this)
    this.handleAuthStateChange = this.handleAuthStateChange.bind(this)
    this.handleQuarterChange = this.handleQuarterChange.bind(this)
    this.handleChangePercViews = this.handleChangePercViews.bind(this)
  }

  handleAuthStateChange(nextAuthState, data) {
    if (nextAuthState === 'signedout') {
      this.props.logout(data).then(_ => this.props.history.push('/login'))
    }
  }

  handleQuarterChange(event) {
    this.setState({ currentQuarter: event.target.value });
  }

  handleChangePercViews(event) {
    this.setState({ percViews: event.target.value });
  }

  loadData(quarter) {
    const cq = quarter.toUpperCase();
    const q = cq.substring(0, 2);
    const y = cq.substring(2, 6);
    var postData = {
      path: 'getFile',
      data: {
        bucket: 'big-data-media',
        prefix: `crm/rfv_auvio_segments/${y}/${q}/${cq}_entire_data.json`,
        user: {
          email: this.props.user.attributes.email,
          poolId: this.props.user.pool.userPoolId,
          clientId: this.props.user.pool.clientId,
          tokenId: this.props.user.signInUserSession.idToken.jwtToken,
          refreshToken: this.props.user.signInUserSession.refreshToken.token,
          accessToken: this.props.user.signInUserSession.accessToken.jwtToken
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.props.post(postData).then(response => {
        if (response['data'])
          resolve(response['data'])
        else if (response['Error'])
          reject(response['Error'])
      })
        .catch(error => reject(error))
    })
  }

  componentDidMount() {
    if (!this.props.user || !this.props.user.attributes || !this.props.user.attributes.email)
      this.props.history.push('/login');
    else {
      this.setState({ percViews: '80' })

      const today = new Date()
      const startDate = new Date(2020, 0, 1);
      const endDate = getLastDayQuarter(today).addDays(1)

      let last = new Date(startDate);
      let lastQ = '';
      let quarters = [];

      while (last < endDate) {
        let quarter = `q${getQuarter(last)}${last.getFullYear()}`;
        if (quarter !== lastQ) {
          lastQ = quarter;
          quarters.push(quarter);
        }
        last = new Date(last.setMonth(last.getMonth() + 1));
      }

      this.setState({ quarters: quarters })
    }
  }

  componentDidUpdate(_, prevState) {
    if (this.state.quarters && ((prevState.quarters !== this.state.quarters) || (prevState.quarters.length !== this.state.quarters.length))) {
      var currentQuarter = this.state.quarters[this.state.quarters.length - 1]
      this.setState({ currentQuarter: currentQuarter })
    }
    else if ((prevState.currentQuarter !== this.state.currentQuarter) && this.state.currentQuarter) {
      this.loadData(this.state.currentQuarter).then(data => {
        this.setState({ fullNetData: data['network'] })
        this.setState({ summaryData: data['summary'] })
        this.setState({ lastModified: data['last_modified'] })
      })
        .catch(_ => {
          let newQuarters = this.state.quarters.slice(0, -1)
          this.setState({ quarters: newQuarters })
        })
    }
    else if (((prevState.percViews !== this.state.percViews) && this.state.percViews) || (this.state.summaryData && this.state.fullNetData && this.state.summaryData[this.state.currentQuarter] && this.state.fullNetData[this.state.currentQuarter])) {
      this.createHeatMap()
      this.createSegmentsDistribution()
      Object.keys(this.state.segmentColors).slice(1).map((d, i) => (
        createSocioDemo(
          d,
          i,
          this.state.summaryData[this.state.currentQuarter]['sociodemo'],
          d,
          this.state.summaryData[this.state.currentQuarter][d].frequency.count,
          d,
          this.state.summaryData[this.state.currentQuarter]['sociodemo'],
          this.state.genderColor
        )
      ))
      this.state.segments.map(async d => {
        this.createNetwork(d, this.state.fullNetData[this.state.currentQuarter][this.state.percViews][d.normalize("NFD").replace(/[\u0300-\u036f]/g, "")])
        await this.createArcDiagram(d, this.state.fullNetData[this.state.currentQuarter][this.state.percViews][d.normalize("NFD").replace(/[\u0300-\u036f]/g, "")])
      })
    }
  }

  getGroups = () => {
    let groups = ["fiction", "information", "sport", "entertainment", "lifestyle", "knowledge", "default"]
    let arrays = Object.values(this.state.fullNetData[this.state.currentQuarter][this.state.percViews])
      .map((d) => Object.values(d))
      .flat()
      .filter((e) => {
        return e !== "" && e.length > 0;
      })
    let allGroups = [].concat.apply([], arrays).map((d) => d.grp)
    let setGroups = ([...groups, ...allGroups])
    return [...new Set(setGroups)]
  }

  createHeatMap = () => {
    let data = this.state.summaryData[this.state.currentQuarter]['summary']
    var margin = { top: 0, right: 50, bottom: 50, left: 0 };
    var width = 500 - margin.left - margin.right;
    var height = 500 - margin.top - margin.bottom;
    var minUsers = d3.min(Object.values(this.state.summaryData).map(d => d3.min(d.summary, e => e.users)))
    var maxUsers = d3.max(Object.values(this.state.summaryData).map(d => d3.max(d.summary, e => e.users)))

    var svg = d3.select('#heatmap').html('').append('svg');
    svg
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3
      .scaleBand()
      .domain([0, 1, 2])
      .range([margin.left, width]);

    var y = d3
      .scaleBand()
      .domain([0, 1, 2])
      .range([margin.top, height]);

    var size = d3
      .scaleLinear()
      .domain([minUsers, maxUsers])
      .range([30, height / 3]);

    let allUsers = d3.sum(data, d => d.users);

    svg
      .append("g")
      .attr("transform", `translate(${margin.right},${height})`)
      .call(
        d3
          .axisBottom(x)
          .tickFormat(
            x => ['Il y a longetemps', 'Récemment', 'Très récemment'][x]
          )
      )
      .call(g => g.select(".domain").remove());

    svg
      .append("g")
      .attr("transform", "translate(" + width + "," + margin.top + ")")
      .call(d3.axisRight(y).tickFormat(x => ['Élevé', 'Moyen', 'Bas'][x]))
      .call(g => g.select(".domain").remove());

    let groups = svg
      .selectAll('g.segments')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'segments');

    groups
      .append('rect')
      .attr('x', d => x(d.recencyCluster))
      .attr('y', d => y(d.frequencyCluster))
      .attr('width', height / 3)
      .attr('height', height / 3)
      .style('fill', '#fff')
      .style('stroke', '#eee');

    groups
      .append('rect')
      .attr('x', d => x(d.recencyCluster) + (height / 3 - size(d.users)) / 2)
      .attr('y', d => y(d.frequencyCluster) + (height / 3 - size(d.users)) / 2)
      .attr('width', d => size(d.users))
      .attr('height', d => size(d.users))
      .style('fill', d => this.state.segmentColors[d.segment]);

    groups
      .append('text')
      .attr('x', d => x(d.recencyCluster) + height / 3 / 2)
      .attr('y', d => y(d.frequencyCluster) + height / 3 / 2)
      .style("text-anchor", "middle")
      .style("font-size", "13px")
      .style("font-weight", "bold")
      .style("fill", d => (d.segment === 'occasionnels' ? 'black' : 'white'))
      .text(d => `${parseInt(d.users / 1000)}k`);

    groups
      .append('text')
      .attr('x', d => x(d.recencyCluster) + height / 3 / 2)
      .attr('y', d => y(d.frequencyCluster) + height / 3 / 2 + 12)
      .style("text-anchor", "middle")
      .style("font-size", "13px")
      .style("font-weight", "bold")
      .style("fill", d => (d.segment === 'occasionnels' ? 'black' : 'white'))
      .text(d => d3.format(".0%")(d.users / allUsers));

    svg
      .append("text")
      .attr('x', margin.left)
      .attr('y', height)
      .style("text-anchor", "start")
      .style("font-size", "10px")
      .attr("font-weight", "bold")
      .text("Vu pour la dernière fois");

    svg
      .append("text")
      .attr('x', width)
      .attr('y', 10)
      .style("text-anchor", "middle")
      .style("font-size", "10px")
      .attr("font-weight", "bold")
      .text("Contenu consommé");

    return svg.node();
  }

  createSegmentsDistribution = () => {
    var data = this.state.summaryData[this.state.currentQuarter]
    var margin = { top: 0, right: 30, bottom: 20, left: 300 };
    var width = (this.state.width > 1000 ? 1000 : this.state.width) - margin.left - margin.right;
    var height = 200 - margin.top - margin.bottom;

    var svg = d3.select('#segmentsDistribution').html('').append('svg');
    svg
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var xF = d3
      .scaleLinear()
      .domain([0, d3.max(this.state.segments.map(d => data[d].frequency.mean)) * 1.1])
      .range([margin.left, width]);

    svg
      .append("g")
      .attr("transform", "translate(0," + 120 + ")")
      .call(d3.axisBottom(xF));

    svg
      .append("text")
      .attr('x', 10)
      .attr('y', 20)
      .text("Nb d'utilisateurs");

    svg
      .append("text")
      .attr('x', 10)
      .attr('y', 70)
      .text('Jours depuis dernière connection');

    svg
      .append("text")
      .attr('x', 10)
      .attr('y', 120)
      .text('Nb moyen contenu consommé');

    svg
      .append("text")
      .attr('x', 10)
      .attr('y', 170)
      .text('Jours en ligne');

    let groupsF = svg
      .selectAll('.frequency')
      .data(this.state.segments)
      .enter()
      .append('g')
      .attr('class', 'frequency');

    groupsF
      .append('circle')
      .attr('cx', d => xF(data[d].frequency.mean))
      .attr('cy', 120)
      .attr('r', 20)
      .style('fill', d => this.state.segmentColors[d])
      .style('opacity', 0.9);

    groupsF
      .append('text')
      .attr('x', d => xF(data[d].frequency.mean))
      .attr('y', 120)
      .style('dominant-baseline', 'middle')
      .style('text-anchor', 'middle')
      .text(d => parseInt(data[d].frequency.mean))
      .style('fill', d => (d === 'occasionnels' ? 'black' : 'white'));

    var xR = d3
      .scaleLinear()
      .domain([0, d3.max(this.state.segments.map(d => data[d].recency.mean)) * 1.1])
      .range([margin.left, width]);

    svg
      .append("g")
      .attr("transform", "translate(0," + 70 + ")")
      .call(d3.axisBottom(xR));

    let groupsR = svg
      .selectAll('.recency')
      .data(this.state.segments)
      .enter()
      .append('g')
      .attr('class', 'recency');

    groupsR
      .append('circle')
      .attr('cx', d => xR(data[d].recency.mean))
      .attr('cy', 70)
      .attr('r', 20)
      .style('fill', d => this.state.segmentColors[d])
      .style('opacity', 0.9);

    groupsR
      .append('text')
      .attr('x', d => xR(data[d].recency.mean))
      .attr('y', 70)
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'middle')
      .text(d => parseInt(data[d].recency.mean))
      .style('fill', d => (d === 'occasionnels' ? 'black' : 'white'));

    var xD = d3
      .scaleLinear()
      .domain([0, d3.max(this.state.segments.map(d => data[d].days.mean)) * 1.1])
      .range([margin.left, width]);

    svg
      .append("g")
      .attr("transform", "translate(0," + 170 + ")")
      .call(d3.axisBottom(xD));

    let groupsD = svg
      .selectAll('.days')
      .data(this.state.segments)
      .enter()
      .append('g')
      .attr('class', 'days');

    groupsD
      .append('circle')
      .attr('cx', d => xD(data[d].days.mean))
      .attr('cy', 170)
      .attr('r', 20)
      .style('fill', d => this.state.segmentColors[d])
      .style('opacity', 0.9);

    groupsD
      .append('text')
      .attr('x', d => xD(data[d].days.mean))
      .attr('y', 170)
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'middle')
      .text(d => parseInt(data[d].days.mean))
      .style('fill', d => (d === 'occasionnels' ? 'black' : 'white'));

    var xC = d3
      .scaleLinear()
      .domain([0, d3.max(this.state.segments.map(d => data[d].frequency.count)) * 1.1])
      .range([margin.left, width]);

    svg
      .append("g")
      .attr("transform", "translate(0," + 20 + ")")
      .call(d3.axisBottom(xC));

    let groupsC = svg
      .selectAll('.count')
      .data(this.state.segments)
      .enter()
      .append('g')
      .attr('class', 'days');

    groupsC
      .append('circle')
      .attr('cx', d => xC(data[d].frequency.count))
      .attr('cy', 20)
      .attr('r', 20)
      .style('fill', d => this.state.segmentColors[d])
      .style('opacity', 0.9);

    groupsC
      .append('text')
      .attr('x', d => xC(data[d].frequency.count))
      .attr('y', 20)
      .style('text-anchor', 'middle')
      .style('dominant-baseline', 'middle')
      .text(d => `${parseInt(data[d].frequency.count / 1000)}k`)
      .style('fill', d => (d === 'occasionnels' ? 'black' : 'white'));

    return svg.node();
  }

  createNetwork = (id, data) => {
    var margin = { top: 0, right: 0, bottom: 0, left: 0 };
    var width = (this.state.width > 800 ? 800 : this.state.width) - margin.left - margin.right;
    var height = (this.state.width > 450 ? 450 : this.state.width) - margin.top - margin.bottom;

    var sociodemo = this.state.summaryData[this.state.currentQuarter]['sociodemo']
    var genderColor = this.state.genderColor

    var color = d3.scaleOrdinal()
      .domain(this.getGroups())
      .range(this.state.fikelsColors);

    const links = data.links.map(d => Object.create(d));
    const nodes = data.nodes.map(d => Object.create(d));

    const size = getSizeScale(data);
    let idToNode = getIdToNodeObj(data);

    const simulation = d3
      .forceSimulation(nodes)
      .force("link", d3.forceLink(links).id(d => d.id))
      .force("charge", d3.forceManyBody().strength(-500))
      .force("center", d3.forceCenter(width / 2, height / 2));
    // .alphaMin(1)

    const svg = d3.select(`#net_${id}`).html('')//.attr("viewBox", [0, 0, width, height])
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const tooltip = svg.append("g").attr('id', `tooltip_${id}`).attr("viewBox", [0, 0, width, height]);

    const link = svg
      .append("g")
      .attr("stroke", "#999")
      .attr("stroke-opacity", 0.6)
      .selectAll("line")
      .data(links)
      .join("line")
      .attr("stroke-width", 1);

    const node = svg
      .append("g")
      .attr("stroke", "#fff")
      .attr('stroke-opacity', 1)
      .attr("stroke-width", 1.5)
      .selectAll("circle")
      .data(nodes)
      .join("circle")
      .attr("class", d => `circle_${hashCode(d.id)}`)
      .attr("r", d => size(d.n))
      .attr("fill", d => color(d.grp))
      .call(drag(simulation));

    const text = svg
      .append("g")
      .selectAll("text")
      .data(nodes)
      .join("text")
      .attr("class", d => `text_${hashCode(d.id)}`)
      .style('font-size', '8px');

    node.append("title").text(d => `${d.id} (${d3.format(',')(d.n)})`);

    simulation.on("tick", () => {
      link
        .attr("x1", d => d.source.x)
        .attr("y1", d => d.source.y)
        .attr("x2", d => d.target.x)
        .attr("y2", d => d.target.y);

      node.attr("cx", d => d.x).attr("cy", d => d.y);
      text.attr("x", d => d.x + 10).attr("y", d => d.y);
    });

    node
      .on('mouseover', function (d) {
        node.style('opacity', .2);
        text
          .style('font-weight', _textd_ =>
            d.target.__data__.index === _textd_.index ? 'bold' : ''
          )
          .style('font-size', _textd_ =>
            d.target.__data__.index === _textd_.index ? '10px' : '8px'
          )
          .text(_textd_ =>
            d.target.__data__.index === _textd_.index ? `${d.target.textContent}` : ''
          );
        d3.select(this).style('opacity', 1);
        link
          .attr('stroke', link_d => {
            if (link_d.source.index === d.target.__data__.index) {
              svg
                .selectAll(`.text_${hashCode(link_d.target.index)}`)
                .text(e => `${e.index}`);
              svg
                .selectAll(`.circle_${hashCode(link_d.target.index)}`)
                .style('opacity', 1);
              return '#bbb';
            }

            if (link_d.target.index === d.target.__data__.index) return '#bbb';
            return '#b8b8b8';
          })
          .attr('stroke-opacity', link_d =>
            link_d.source.index === d.target.__data__.index || link_d.target.index === d.target.__data__.index
              ? 1
              : 0
          )
          .attr('stroke-width', link_d =>
            link_d.source.index === d.target.__data__.index || link_d.target.index === d.target.__data__.index
              ? 1
              : 0
          );
        let nodeData =
          idToNode[this.innerHTML.substring(7, this.innerHTML.indexOf('(') - 1)];
        createSocioDemo(`tooltip_${id}`, 0, nodeData['sociodemo'], '', nodeData.n, undefined, sociodemo, genderColor);
      })
      .on('mouseout', function (d) {
        tooltip.html('');
        node.style('opacity', 1);
        text.style('font-size', '8px').text('');
        link
          .attr("stroke", "#999")
          .attr("stroke-opacity", 0.6)
          .attr('stroke-width', 1);
      });

    return svg.node();
  }

  createArcDiagram = async (id, data) => {
    var margin = { top: 0, right: 30, bottom: 200, left: 60 };
    var width = (this.state.width > 1000 ? 1000 : this.state.width) - margin.left - margin.right;
    var height = 700 - margin.top - margin.bottom;

    let strokeWidthScales = await getStrokeWidthScale(data);
    let idToNode = getIdToNodeObj(data);

    var color = d3.scaleOrdinal()
      .domain(this.getGroups())
      .range(this.state.fikelsColors);

    // append the svg object to the body of the page
    const svg = d3.select(`#arc_${id}`).html('')

    svg
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // List of node names
    var allNodes = data.nodes.map(function (d) {
      return d.name;
    });

    var size = getSizeScale(data);

    // A linear scale to position the nodes on the X axis
    var x = d3
      .scalePoint()
      .range([margin.left, width])
      .domain(allNodes);

    // Add the links
    var links = svg
      .selectAll('mylinks')
      .data(data.links)
      .enter()
      .append('path')
      .attr('d', function (d) {
        let start = x(idToNode[d.source].name);
        let end = x(idToNode[d.target].name);
        return [
          'M',
          start,
          height - 30,
          'A',
          (start - end) / 2,
          ',',
          (start - end) / 2,
          0,
          0,
          ',',
          start < end ? 1 : 0,
          end,
          ',',
          height - 30
        ].join(' ');
      })
      .style("fill", "none")
      .attr("stroke", "grey")
      .style("stroke-width", d => strokeWidthScales[d.source](d.value));

    // Add the circle for the nodes
    var nodes = svg
      .selectAll("mynodes")
      .data(
        data.nodes.sort(function (a, b) {
          return +b.n - +a.n;
        })
      )
      .enter()
      .append("circle")
      .attr("cx", function (d) {
        return x(d.name);
      })
      .attr("cy", height - 30)
      .attr("r", function (d) {
        return size(d.n);
      })
      .style("fill", function (d) {
        return color(d.grp);
      })
      .attr("stroke", "white");

    // And give them a label
    var labels = svg
      .selectAll("mylabels")
      .data(data.nodes)
      .enter()
      .append("text")
      .attr("x", 0)
      .attr("y", 0)
      .text(function (d) {
        return d.name;
      })
      .style("text-anchor", "end")
      .style("font-weight", '300')
      // .style("font-family", "'Yanone Kaffeesatz', sans-serif")
      .attr("transform", function (d) {
        return "translate(" + x(d.name) + "," + (height - 15) + ")rotate(-45)";
      })
      .style("font-size", '6px');

    // Add the highlighting functionality
    nodes
      .on('mouseover', function (d) {
        nodes.style('opacity', .2);
        d3.select(this).style('opacity', 1);
        links
          .style('stroke', function (link_d) {
            return link_d.source === d.target.__data__.name
              ? color(idToNode[link_d.target].grp)
              : '#b8b8b8';
          })
          .style('stroke-opacity', function (link_d) {
            return link_d.source === d.target.__data__.name ? 1 : 0;
          })
          .style('stroke-width', function (link_d) {
            return link_d.source === d.target.__data__.name
              ? 4 * strokeWidthScales[link_d.source](link_d.value)
              : strokeWidthScales[link_d.source](link_d.value);
          });
        labels
          .style("font-size", function (label_d) {
            return label_d.name === d.target.__data__.name ? '16px' : '6px';
          })
          .attr("y", function (label_d) {
            return label_d.name === d.target.__data__.name ? 10 : 0;
          });
      })
      .on('mouseout', function (d) {
        nodes.style('opacity', 1);
        links
          .style('stroke', 'grey')
          .style('stroke-opacity', .8)
          .style('stroke-width', d => strokeWidthScales[d.source](d.value));
        labels.attr("y", 0).style("font-size", '6px');
      });
    return svg.node();
  }

  render() {
    if ((this.state.lastModified) && (this.state.currentQuarter != '') && (this.state.summaryData) && (this.state.summaryData[this.state.currentQuarter])) {
      return (
        <div id="wrapper" className="container center-align">
          <h1>Segmentation d'utilisateurs par leur récence et fréquence des visions sur Auvio</h1>
          <Author width={this.state.width} name='Meili Vanegas-Hernandez' icon='https://avatars.githubusercontent.com/u/10857734?v=4' link='https://www.linkedin.com/in/meilivh/' date='Jul 15 2021'></Author>
          <div>
            <div className="row"><label>Choisissez un trimestre</label></div>
            <div className="row"><select style={{ 'display': 'inline', 'width': 'auto' }} value={this.state.currentQuarter} onChange={this.handleQuarterChange}>{this.state.quarters.map((d, i) => <option key={`option.${d}`} value={`${d}`}>{d}</option>)}</select></div>
          </div>
          <br/>
          <h2>{`Utilisateurs distincts atteints en ${this.state.currentQuarter.substring(0, 2).toUpperCase()} ${this.state.currentQuarter.substr(2).toUpperCase()} : ${d3.format(',')(d3.sum(this.state.summaryData[this.state.currentQuarter]['summary'], d => d.users))}`}</h2>
          <br/>
          <p>*Dernière mise à jour le {this.state.lastModified}</p>
          <br/>
          <br/>
          <h2>Distribution des segments par leur récence et fréquence des visions sur Auvio</h2>
          <br/>
          <Swatches colors={Object.values(this.state.segmentColors).slice(1)} text={Object.keys(this.state.segmentColors).slice(1)}></Swatches>
          <br/>
          <div id="heatmap"></div>
          <br/>
          <h2>Les segments en chiffres</h2>
          <br/>
          <div id="segmentsDistribution"></div>
          <br/>
          <h2>Distribution socio-démographique des segments</h2>
          <br/>
          <h3>Genre</h3>
          <br/>
          <div>
            <Swatches text={this.state.genderColor.text} colors={this.state.genderColor.colors}></Swatches>
          </div>
          <br/>
          <svg id='fidèles'></svg>
          <svg id='reconnus'></svg>
          <svg id='occasionnels'></svg>
          <br/>
          <div>
            <label>Pourcentage des vues à considerer pour la création des graphes</label>
            <br/>
            <form action="#">
              <p><label><input value="80" type="radio" onChange={this.handleChangePercViews} checked={this.state.percViews === "80"}/>80%</label></p><br></br>
              <p><label><input value="90" type="radio" onChange={this.handleChangePercViews} checked={this.state.percViews === "90"}/>90%</label></p>
            </form>
          </div>
          <br/>
          <h2>Programmes qui atteignent 80% des vues du segment</h2>
          <br/>
          <h3>{`${this.state.currentQuarter.substring(0, 2).toUpperCase()} ${this.state.currentQuarter.substr(2).toUpperCase()} Fidèles`}</h3>
          <br/>
          <div className="row"><Swatches text={this.getGroups()} colors={this.state.fikelsColors}></Swatches></div>
          <div className="row"><svg id='net_fidèles'></svg></div>
          <div className="row"><svg id='arc_fidèles'></svg></div>
          <br/>
          <h3>{`${this.state.currentQuarter.substring(0, 2).toUpperCase()} ${this.state.currentQuarter.substr(2).toUpperCase()} Reconnus`}</h3>
          <br/>
          <div className="row"><Swatches text={this.getGroups()} colors={this.state.fikelsColors}></Swatches></div>
          <div className="row"><svg id='net_reconnus'></svg></div>
          <div className="row"><svg id='arc_reconnus'></svg></div>
          <br/>
          <h3>{`${this.state.currentQuarter.substring(0, 2).toUpperCase()} ${this.state.currentQuarter.substr(2).toUpperCase()} Occasionnels`}</h3>
          <br/>
          <div className="row"><Swatches text={this.getGroups()} colors={this.state.fikelsColors}></Swatches></div>
          <div className="row"><svg id='net_occasionnels'></svg></div>
          <div className="row"><svg id='arc_occasionnels'></svg></div>
        </div>
      );
    }
    else return ((
      <div id="wrapper" className="container center-align">
        <br /><br />
        <Loader></Loader>
      </div>))
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    post: (params) => dispatch(post(params))
  };
};

Segments = connect(mapStateToProps, mapDispatchToProps)(Segments);

export default withRouter(Segments);
