import React, { Component } from "react";
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import Site from '../components/Site.jsx'
import * as d3 from 'd3'

var sites = [
  { path: "segmentationAuvio", title: "Segmentation d'utilisateurs par leur récence et fréquence des visions sur Auvio"},
  { path: "mySegments", title: "Quels sont mes segments ?" },
  { path: 'crmStats', title: 'Segments d\'affinité : les chiffres' },
  { path: 'addSegment', title: 'CRM : Ajout de mappings entre les segments et le contenu' }
]

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sites: sites,
      scale: d3.scaleOrdinal().domain(sites.map(d => d.path)).range(d3.schemePaired.slice(0, 2))
    };
  }

  componentDidMount() {
    if (!this.props.user || !this.props.user.username)
      this.props.history.push('/login');
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>
            <span style={{ display: 'block' }}>Journal des projets dataviz</span>
          </h1>
          <br></br>
          <br></br>
          <div className="container">
            {this.state.sites.map(d => <Site key={d.path} title={d.title} onClick={() => this.props.history.push(`/${d.path}`)} color={this.state.scale(d.path)}></Site>)}
          </div>
        </header>
      </div>)
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateUtilisationApproval: (params) =>
      dispatch(updateUtilisationApproval(params)),
  };
};

Home = connect(mapStateToProps, mapDispatchToProps)(Home);

export default withRouter(Home);
