import React from 'react';
import * as d3 from 'd3'
import Swatches from '../components/Swatches.jsx'
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Author from '../components/Author.jsx';
import { post } from '../actions/restAction'
import Loader from '../components/Loader.jsx';


class CRMSegmentsStats extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      width: document.body.clientWidth * 0.9,
      recency: undefined,
      recOptions: [
        { value: 7, label: "Vue au cours des 7 derniers jours" },
        { value: 30, label: "Vue au cours des 30 derniers jours" },
        { value: 90, label: "Vue au cours des 90 derniers jours" },
        { value: 180, label: "Vue au cours des 180 derniers jours" },
        { value: 365, label: "Vue au cours des 365 derniers jours" }
      ],
      segColors: {
        text: [
          "fiction",
          "information",
          "sport",
          "entertainment",
          "lifestyle",
          "knowledge",
          "default"
        ],
        colors: [
          "#8b1e3f",
          "#197BBD",
          "#89bd9e",
          "#f0c987",
          "#FE938C",
          '#9F7E69',
          '#fd8d3c'
        ]
      },
      colors: {
        Information: "#197BBD",
        TV: '#f0c987',
        Sport: "#89bd9e",
        Fiction: "#8b1e3f",
        Knowledge: '#9F7E69',
        Entertainment: "#f0c987",
        Vivacite: "#f0c987",
        LaPremiere: "#9F7E69",
        Moteurs: "#89bd9e",
        Football: "#89bd9e",
        RelaxJA: "#FE938C",
        Lifestyle: '#FE938C',
        Documentaires: '#9F7E69',
        Cyclisme: '#89bd9e',
        Films: '#8b1e3f',
        SeriesNous: '#8b1e3f',
        Investigation: '#9F7E69',
        Formule1: '#89bd9e',
        SeriesCorner: '#8b1e3f',
        Culture: '#9F7E69',
        Classic21: "#9F7E69",
        Consommation: '#FE938C',
        Histoire: '#9F7E69',
        MotoGP: '#89bd9e',
        DiablesRouges: '#89bd9e',
        AB3: '#FE938C',
        Archives: '#9F7E69',
        Humour: '#f0c987',
        Tennis: '#89bd9e',
        EuropaLeague: '#89bd9e',
        Musiq3: '#9F7E69',
        ABX: '#FE938C',
        WRC: '#89bd9e',
        Patrimoine: '#FE938C',
        Nature: '#FE938C',
        Basket: '#89bd9e',
        Voyage: '#FE938C',
        Jardinage: '#FE938C',
        ArtDeVivre: '#FE938C',
        Cuisine: '#FE938C',
        Athletisme: '#89bd9e',
        CycloCross: '#89bd9e',
        eSport: '#89bd9e',
        AuvioKids: '#8b1e3f',
        MusiqueClassique: '#9F7E69',
        Webcreation: '#8b1e3f',
        AuvioColors: '#8b1e3f',
        Tarmac: '#FE938C',
        Hockey: '#89bd9e',
        Rugby: '#89bd9e'
      },
      lastModified: undefined,
      users: undefined,
      barData: undefined,
      chordData: undefined,
      distribution: undefined
    }

    this.loadData = this.loadData.bind(this)
    this.handleAuthStateChange = this.handleAuthStateChange.bind(this)
    this.handleRecencyChange = this.handleRecencyChange.bind(this)
    this.createScatterPlot = this.createScatterPlot.bind(this)
    this.createBarChart = this.createBarChart.bind(this)
    this.createChordDiagram = this.createChordDiagram.bind(this)
  }

  handleAuthStateChange(nextAuthState, data) {
    if (nextAuthState === 'signedout') {
      this.props.logout(data).then(_ => this.props.history.push('/login'))
    }
  }

  handleRecencyChange(event) {
    this.setState({ recency: event.target.value });
  }

  loadData(recency) {
    var postData = {
      path: 'getFile',
      data: {
        bucket: 'big-data-media',
        prefix: `crm/in/segments/stats/stats_${recency}.json`,
        user: {
          email: this.props.user.attributes.email,
          poolId: this.props.user.pool.userPoolId,
          clientId: this.props.user.pool.clientId,
          tokenId: this.props.user.signInUserSession.idToken.jwtToken,
          refreshToken: this.props.user.signInUserSession.refreshToken.token,
          accessToken: this.props.user.signInUserSession.accessToken.jwtToken
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.props.post(postData).then(response => {
        if (response['data'])
          resolve(response['data'])
        else if (response['Error'])
          reject(response['Error'])
      })
        .catch(error => reject(error))
    })
  }

  componentDidMount() {
    if (!this.props.user || !this.props.user.attributes || !this.props.user.attributes.email)
      this.props.history.push('/login');
    else
      this.setState({ recency: 7 })
  }

  componentDidUpdate(_, prevState) {
    if (this.state.recency && (prevState.recency != this.state.recency)) {
      this.loadData(this.state.recency).then(data => {

        var chordData = Object.assign(data['values'], {
          names: data['names'],
          colors: data.names.map(d => this.state.colors[d])
        })

        this.setState({ lastModified: data['last_modified'] })
        this.setState({ users: data['count'] })
        this.setState({ barData: Object.assign(data['counts'].sort((a, b) => d3.descending(a.value, b.value)), { format: ",.0f" }) })
        this.setState({ chordData: chordData })
        this.setState({ distribution: data['distribution'].filter(d => d.segs !== 0) })

      })
    }
    else if (this.state.barData && this.state.chordData && this.state.distribution) {
      this.createScatterPlot(this.state.distribution)
      this.createBarChart(this.state.barData)
      this.createChordDiagram(this.state.chordData)
    }

  }

  createScatterPlot(data) {
    var margin = { top: 20, right: 85, bottom: 50, left: 50 }

    var width = (this.state.width > 600 ? 600 : this.state.width) - margin.left - margin.right
    var height = (this.state.width > 600 ? 600 : this.state.width) * 0.6 - margin.top - margin.bottom

    const xAttr = 'segs'
    const yAttr = 'counts'

    const svg = d3.select("#scatter").html('')
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const x = d3
      .scaleLinear()
      .domain(d3.extent(data, d => d[xAttr]))
      .range([0, width])
      .nice()

    const y = d3
      .scaleLinear()
      .domain(d3.extent(data, d => d[yAttr]))
      .range([height, 0])
      .nice()

    const g = svg
      .append("g")
      .attr("transform", `translate(${margin.left}, ${margin.top})`);

    g.append("g")
      .call(d3.axisBottom(x))
      .attr("transform", `translate(0, ${height})`)
      .call(axis =>
        axis
          .append("text")
          .text('No. segments')
          .style("fill", "black")
          .attr("transform", `translate( ${width}, -10)`)
          .style("text-anchor", "end")
      );
    g.append("g")
      .call(d3.axisLeft(y))
      .call(axis =>
        axis
          .append("text")
          .text('Utilisateurs')
          .style("fill", "black")
          .style("text-anchor", "middle")
          .attr("y", -15)
      );

    g.selectAll("circle")
      .data(data)
      .enter()
      .append("circle")
      .attr("cx", d => x(d[xAttr]))
      .attr("cy", d => y(d[yAttr]))
      .attr("r", 4)
      .style("fill", 'grey');

    return svg.node();
  }

  createBarChart(data) {
    var margin = { top: 30, right: 0, bottom: 10, left: 50 }

    var width = (this.state.width > 600 ? 600 : this.state.width)
    var barHeight = 25

    var height = Math.ceil((data.length + 0.1) * barHeight) + margin.top + margin.bottom

    var x = d3.scaleLinear()
      .domain([0, d3.max(data, d => d.value)])
      .range([margin.left, width - margin.right])

    var y = d3.scaleBand()
      .domain(d3.range(data.length))
      .rangeRound([margin.top, height - margin.bottom])
      .padding(0.1)

    var xAxis = g => g
      .attr("transform", `translate(0,${margin.top})`)
      .call(d3.axisTop(x).ticks(width / 80, data.format))
      .call(g => g.select(".domain").remove())

    var yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y).tickFormat(i => data[i].name).tickSizeOuter(0))

    var format = x.tickFormat(20, data.format)

    const svg = d3.select("#barchart").html('')
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


    svg.append("g")
      .attr("fill", "steelblue")
      .selectAll("rect")
      .data(data)
      .join("rect")
      .attr("x", x(0))
      .attr("y", (d, i) => y(i))
      .attr("fill", 'gray')
      .attr("width", d => x(d.value) - x(0))
      .attr("height", y.bandwidth());

    svg.append("g")
      .attr("fill", "white")
      .attr("text-anchor", "end")
      .attr("font-family", "sans-serif")
      .attr("font-size", '12px')
      .selectAll("text")
      .data(data)
      .join("text")
      .attr("x", d => x(d.value))
      .attr("y", (d, i) => y(i) + y.bandwidth() / 2)
      .attr("dy", "0.35em")
      .attr("dx", -4)
      .text(d => format(d.value))
      .call(text => text.filter(d => x(d.value) - x(0) < 20) // short bars
        .attr("dx", +4)
        .attr("fill", "black")
        .attr("text-anchor", "start"));

    svg.append("g")
      .call(xAxis);

    svg.append("g")
      .call(yAxis);

    return svg.node();
  }

  createChordDiagram(data) {
    var width = this.state.width > 850 ? 850 : this.state.width
    var height = width

    const names = data.names === undefined ? d3.range(data.length) : data.names
    const colors = data.colors === undefined ? d3.quantize(d3.interpolateRainbow, names.length) : data.colors

    var color = d3.scaleOrdinal(names, colors)

    var outerRadius = Math.min(width, height) * 0.5 - 60

    var innerRadius = outerRadius - 10

    var tickStep = d3.tickStep(0, d3.sum(data.flat()), 100)
    var formatValue = d3.format(".3s")

    function ticks({ startAngle, endAngle, value }) {
      const k = (endAngle - startAngle) / value;
      return d3.range(0, value, tickStep).map(value => {
        return { value, angle: value * k + startAngle };
      });
    }

    var chord = d3.chord()
      .padAngle(10 / innerRadius)
      .sortSubgroups(d3.descending)
      .sortChords(d3.descending)

    var arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)

    var ribbon = d3.ribbon()
      .radius(innerRadius - 1)
      .padAngle(1 / innerRadius)


    const svg = d3.select("#chordDiagram").html('')
      .attr("viewBox", [-width / 2, -height / 2, width, height]);

    const chords = chord(data);

    const group = svg.append("g")
      .attr("font-size", '10px')
      .attr("font-family", "sans-serif")
      .selectAll("g")
      .data(chords.groups)
      .join("g");

    group.append("path")
      .attr("fill", d => color(names[d.index]))
      .attr("d", arc);

    group.append("title")
      .text(d => `${names[d.index]}
  ${formatValue(d.value)}`);

    const groupTick = group.append("g")
      .selectAll("g")
      .data(ticks)
      .join("g")
      .attr("transform", d => `rotate(${d.angle * 180 / Math.PI - 90}) translate(${outerRadius},0)`);

    groupTick.append("line")
      .attr("stroke", "currentColor")
      .attr("x2", 6);

    groupTick.append("text")
      .attr("x", 8)
      .attr("dy", "0.35em")
      .attr("transform", d => d.angle > Math.PI ? "rotate(180) translate(-16)" : null)
      .attr("text-anchor", d => d.angle > Math.PI ? "end" : null)
      .text(d => formatValue(d.value));

    group.select("text")
      .attr("font-weight", "bold")
      .text(function (d) {
        return this.getAttribute("text-anchor") === "end"
          ? `↑ ${names[d.index]}`
          : `${names[d.index]} ↓`;
      });

    svg.append("g")
      .attr("fill-opacity", 0.8)
      .selectAll("path")
      .data(chords)
      .join("path")
      .style("mix-blend-mode", "multiply")
      .attr("fill", d => color(names[d.source.index]))
      .attr("d", ribbon)
      .append("title")
      .text(d => `${formatValue(d.source.value)} ${names[d.target.index]} → ${names[d.source.index]}${d.source.index === d.target.index ? "" : `\n${formatValue(d.target.value)} ${names[d.source.index]} → ${names[d.target.index]}`}`);

    return svg.node();
  }


  render() {
    if (this.state.lastModified && this.state.users && this.state.barData && this.state.chordData) {
      return (
        <div id="wrapper" className="container center-align">

          <h1>Segments d'affinité : les chiffres</h1>
          <Author width={this.state.width} name='Meili Vanegas-Hernandez' icon='https://avatars.githubusercontent.com/u/10857734?v=4' link='https://www.linkedin.com/in/meilivh/' date='Jul 01 2021'></Author>
          <div>
            <div className="row"><label>Choisissez un label</label></div>
            <div className="row"><select style={{ 'display': 'inline', 'width': 'auto' }} value={this.state.recency} onChange={this.handleRecencyChange}>{this.state.recOptions.map((d, i) => <option key={`option.${d.value}`} value={`${d.value}`}>{d.label}</option>)}</select></div>
          </div>
          <h2>Au cours des <b>{this.state.recency} derniers jours</b>, il y a <b>{d3.format(',.0f')(this.state.users)}</b> utilisateurs avec au moins un segment</h2>
          <br></br>
          <p>*Dernière mise à jour le {this.state.lastModified}</p>
          <br></br>
          <br></br>
          <br></br>
          <h3>Nombre d'utilisateurs liés à <i>x</i> segments</h3>
          <div className="row">
            <div className="col m1"></div>
            <div className="col m10">
              <svg id="scatter"></svg>
            </div>
            <div className="col m1"></div>
          </div>
          <h3>Nombre d'utilisateurs par segment vues</h3>
          <div className="row">
            <div className="col m1"></div>
            <div className="col m10">
              <svg id="barchart"></svg>
            </div>
            <div className="col m1"></div>
          </div>
          <h3>Correspondances entre segments</h3>
          <br></br>
          <br></br>
          <div className="row">
            <label>FIKELS</label>
          </div>
          <div className="row">
            <div className="col m2"></div>
            <div className="col m8">
              <Swatches text={this.state.segColors.text} colors={this.state.segColors.colors}></Swatches>
              <br></br>
              <svg id="chordDiagram"></svg>
            </div>
            <div className="col m2"></div>
          </div>
        </div>
      );
    }
    else return ((
      <div id="wrapper" className="container center-align">
        <br /><br />
        <Loader></Loader>
      </div>))
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    post: (params) => dispatch(post(params))
  };
};

CRMSegmentsStats = connect(mapStateToProps, mapDispatchToProps)(CRMSegmentsStats);

export default withRouter(CRMSegmentsStats);
