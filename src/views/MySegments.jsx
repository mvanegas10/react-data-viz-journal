import React from 'react';
import * as d3 from 'd3'
import Swatches from '../components/Swatches.jsx'
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { post } from '../actions/restAction'
import Author from '../components/Author.jsx';
import Loader from '../components/Loader.jsx';


class MySegments extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      width: document.body.clientWidth * 0.9,
      eventDim: {
        height: 46,
        width: (document.body.clientWidth * 0.9 / 2) * 0.8,
        colorWidth: 10
      },
      typeColors: {
        article: "#2a9d8f",
        audio: "#e76f51",
        video: "#f4a261",
        live: "#e9c46a",
        regsource: "#264653"
      },
      data: undefined,
      labels: undefined,
      error: undefined
    }

    this.loadData = this.loadData.bind(this)
    this.handleAuthStateChange = this.handleAuthStateChange.bind(this)
    this.createChart = this.createChart.bind(this)
  }

  handleAuthStateChange(nextAuthState, data) {
    if (nextAuthState === 'signedout') {
      this.props.logout(data).then(_ => this.props.history.push('/login'))
    }
  }

  loadData() {
    var postData = {
      path: 'getUserSegments',
      data: {
        user: {
          email: this.props.user.attributes.email,
          poolId: this.props.user.pool.userPoolId,
          clientId: this.props.user.pool.clientId,
          tokenId: this.props.user.signInUserSession.idToken.jwtToken,
          refreshToken: this.props.user.signInUserSession.refreshToken.token,
          accessToken: this.props.user.signInUserSession.accessToken.jwtToken
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.props.post(postData).then(response => {
        if (response['data'])
          resolve(response['data'])
        else if (response['Error'])
          reject(response['Error'])
      })
        .catch(error => reject(error))
    })
  }

  componentDidMount() {
    if (!this.props.user || !this.props.user.attributes || !this.props.user.attributes.email)
      this.props.history.push('/login');
    else
      this.loadData(this.state.recency).then(data => {
        let labels = new Set(data.map((d) => d.recency))

        this.setState({ data: data })
        this.setState({ labels: labels })
      })
        .catch(error => this.setState({ 'error': error }))
  }

  componentDidUpdate(_) {
    if (this.state.data && this.state.labels) {
      this.createChart()
    }
  }


  createChart() {
    var margin = { top: 0, right: 0, bottom: 0, left: 0 };
    var w = this.state.width - margin.left - margin.right;
    var h =
      (this.state.data.length + 1) * this.state.eventDim.height * 1.1 - margin.top - margin.bottom;

    var x = d3
      .scaleLinear()
      .domain([0, this.state.data.length])
      .range([margin.top + this.state.eventDim.height * 1.1, h]);

    var svg = d3.select("#mySegmentsChart");
    svg
      .attr("width", w + margin.left + margin.right)
      .attr("height", h + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    svg
      .append("line")
      .attr("x1", w / 2)
      .attr("y1", margin.top)
      .attr("x2", w / 2)
      .attr("y2", h)
      .attr("stroke", "black");

    svg
      .append("circle")
      .attr("cx", w / 2)
      .attr("cy", margin.top + 8)
      .attr("r", 8)
      .attr("fill", "#ababab");

    var groups = svg.selectAll("g.event").data(this.state.data).enter().append("g");

    groups
      .append("rect")
      .style("fill", "#eee")
      .attr("x", (d) =>
        ["article", "regsource"].includes(d.type)
          ? margin.left + (w / 2) * 0.1
          : w / 2 + (w / 2) * 0.1
      )
      .attr("y", (d, i) => x(i))
      .attr("width", this.state.eventDim.width)
      .attr("height", this.state.eventDim.height);

    groups
      .append("line")
      .attr("x1", (d) =>
        ["article", "regsource"].includes(d.type)
          ? w / 2 - (w / 2) * 0.1
          : w / 2 + (w / 2) * 0.1
      )
      .attr("y1", (d, i) => x(i) + this.state.eventDim.height / 2)
      .attr("x2", w / 2)
      .attr("y2", (d, i) => x(i) + this.state.eventDim.height / 2)
      .attr("stroke", "black");

    groups
      .append("rect")
      .style("fill", (d) => this.state.typeColors[d.type])
      .attr("x", (d) =>
        ["article", "regsource"].includes(d.type)
          ? w / 2 - (w / 2) * 0.1 - this.state.eventDim.colorWidth
          : w / 2 + (w / 2) * 0.1
      )
      .attr("y", (d, i) => x(i))
      .attr("width", this.state.eventDim.colorWidth)
      .attr("height", this.state.eventDim.height);

    groups
      .append("line")
      .attr("x1", w / 2 - (w / 2) * 0.1 - this.state.eventDim.width)
      .attr("y1", (d, i) => x(i) + (i === 0 ? 0 : this.state.eventDim.height * 1.05))
      .attr("x2", w)
      .attr("y2", (d, i) => x(i) + (i === 0 ? 0 : this.state.eventDim.height * 1.05))
      .attr("stroke", (d, i) =>
        i + 1 < this.state.data.length
          ? d.recency !== this.state.data[i + 1].recency || i === 0
            ? "grey"
            : ""
          : ""
      )
      .attr("stroke-dasharray", "4");

    groups
      .append("text")
      .style("font-weight", "bold")
      .style("font-size", '10px')
      .attr("x", margin.left)
      .attr("y", (d, i) => x(i) + (i === 0 ? 0 : this.state.eventDim.height * 1.05))
      .text((d, i) =>
        i + 1 < this.state.data.length
          ? d.recency !== this.state.data[i + 1].recency || i === 0
            ? this.state.data[i + 1].recency.split("_").join(" ")
            : ""
          : ""
      );

    groups
      .append("text")
      .attr(
        "x",
        (d) =>
          (["article", "regsource"].includes(d.type)
            ? margin.left + (w / 2) * 0.1
            : w / 2 + (w / 2) * 0.1 + this.state.eventDim.colorWidth) + 5
      )
      .attr("y", (d, i) => x(i) + 14)
      .style("font-weight", "bold")
      .style("font-size", '11px')
      .text((d) => `${d.title.slice(0, 80)}`);

    groups
      .append("text")
      .attr(
        "x",
        (d) =>
          (["article", "regsource"].includes(d.type)
            ? margin.left + (w / 2) * 0.1
            : w / 2 + (w / 2) * 0.1 + this.state.eventDim.colorWidth) + 5
      )
      .attr("y", (d, i) => x(i) + 26)
      .style("font-size", '10px')
      .text((d) => `${d.segment.toUpperCase()} - ${d.name}`);

    groups
      .append("text")
      .attr(
        "x",
        (d) =>
          (["article", "regsource"].includes(d.type)
            ? margin.left + (w / 2) * 0.1
            : w / 2 + (w / 2) * 0.1 + this.state.eventDim.colorWidth) + 5
      )
      .attr("y", (d, i) => x(i) + 38)
      .style("font-size", '10px')
      .text((d) =>
        d3.timeFormat("%d/%m/%Y")(d3.timeParse("%Y-%m-%d %H:%M:%S")(d.last_date))
      );

    return svg.node();
  }

  render() {
    if (this.state.data && this.state.labels) {
      return (
        <div id="wrapper" className="container center-align">
          <h1>Quels sont mes segments ?</h1>
          <Author width={this.state.width} name='Meili Vanegas-Hernandez' icon='https://avatars.githubusercontent.com/u/10857734?v=4' link='https://www.linkedin.com/in/meilivh/' date='Sept 20 2021'></Author>
          <h2>{[...new Set(this.state.data.map((d) => d.segment))].length} Segments</h2>
          <br/>
          <Swatches text={Object.keys(this.state.typeColors)} colors={Object.values(this.state.typeColors)}></Swatches>
          <svg id="mySegmentsChart"></svg>
        </div>
      );
    }
    else if (this.state.error)
      return (
        <div id="wrapper" className="container center-align">
          <h3>{this.state.error === 'User not allowed to request method.'? 'Vous n\'êtes pas autorisé à voir cette section. Si vous souhaitez suivre le comportement de vos segments, veuillez courriel un mail à <a href="mailto:gestion.donnes@rtbf.be">gestion.donnes@rtbf.be</a>.': this.state.error}</h3>
        </div>
      )
    else return ((
      <div id="wrapper" className="container center-align">
        <br /><br />
        <Loader></Loader>
      </div>))
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    post: (params) => dispatch(post(params))
  };
};

MySegments = connect(mapStateToProps, mapDispatchToProps)(MySegments);

export default withRouter(MySegments);
