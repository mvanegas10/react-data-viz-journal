import React from 'react';
import Amplify from 'aws-amplify';
import { AmplifyAuthenticator, AmplifyRequireNewPassword, AmplifySignIn } from '@aws-amplify/ui-react';
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components';
import { useDispatch } from 'react-redux'
import { login } from '../actions/authenticationAction'
import awsconfig from '../aws-exports';
import { useHistory } from 'react-router-dom';

Amplify.configure(awsconfig);

let AuthStateApp = () => {
  const [authState, setAuthState] = React.useState();
  const [user, setUser] = React.useState();
  const dispatch = useDispatch()
  const history = useHistory();

  React.useEffect(() => {
    if (authState === AuthState.SignedIn && user)
      history.push('/home');
    else
      return onAuthUIStateChange((nextAuthState, authData) => {
        setAuthState(nextAuthState);
        setUser(authData)
        if (authData && authData.attributes && authData.attributes.email) {
          const logindata = dispatch(login(authData))
          logindata.then(_ => history.push('/home'))
        }
      });
  }, [dispatch]);

  return (
    <AmplifyAuthenticator usernameAlias="email">
      <AmplifySignIn slot="sign-in" usernameAlias="email" headerText="Connectez-vous"
        submitButtonText="Se connecter"
        hideSignUp
        formFields={[
          {
            type: "email",
            label: "Email",
            placeholder: "Introduisez votre email",
            required: true,
          },
          {
            type: "password",
            label: "Mot de passe",
            placeholder: "Introduisez votre mot de passe",
            required: true,
          }
        ]}>
      </AmplifySignIn>
      <AmplifyRequireNewPassword
        headerText="Mettez à jour votre mot de passe"
        slot="require-new-password"
        user={user}
        submitButtonText="Mettre à jour"
      ></AmplifyRequireNewPassword>
    </AmplifyAuthenticator>
  );
}

export default AuthStateApp;