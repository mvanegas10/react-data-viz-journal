import React from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { post } from '../actions/restAction'
import Loader from '../components/Loader.jsx';
import { Modal, Button, Table } from 'react-materialize';

const trigger = <Button className="black">Ajouter</Button>

const getModal = state => (state.modal === 'form') ? <Table><tbody>  <tr><td>  Table</td><td>  {state['table']}</td>  </tr>  <tr><td>  Dimension</td><td>  {state['dimension']}</td>  </tr>  <tr><td>  Variable</td><td>  {state['value']}</td>  </tr>  <tr><td>  Segment</td><td>  {state['segment']}</td>  </tr> </tbody>  </Table>: <h3>Le mappage a été ajoutée correctement !</h3>

class UpdateSegments extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      width: document.body.clientWidth * 0.9,
      modal: 'form',
      segments: undefined,
      tables: undefined,
      dimensions: [],
      segment: undefined,
      table: undefined,
      dimension: undefined,
      value: undefined,
      response: undefined,
      error: undefined
    }

    this.loadData = this.loadData.bind(this)
    this.loadDimensions = this.loadDimensions.bind(this)
    this.handleAuthStateChange = this.handleAuthStateChange.bind(this)
    this.handleSegmentChange = this.handleSegmentChange.bind(this)
    this.handleDimensionChange = this.handleDimensionChange.bind(this)
    this.handleTableChange = this.handleTableChange.bind(this)
    this.handleValueChange = this.handleValueChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSegmentChange(event) {
    this.setState({ modal: 'form' })
    this.setState({ segment: event.target.value });
  }

  handleTableChange(event) {
    this.setState({ table: event.target.value });
  }

  handleDimensionChange(event) {
    this.setState({ dimension: event.target.value });
  }

  handleValueChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit() {
    if (this.state.segment && this.state.dimension && this.state.table && this.state.value && this.state.value !== '') {
      var postData = {
        path: 'addSegmentMapping',
        data: {
          segment: this.state.segment,
          table: this.state.table,
          dimension: this.state.dimension,
          value: this.state.value.trim(),
          user: {
            email: this.props.user.attributes.email,
            poolId: this.props.user.pool.userPoolId,
            clientId: this.props.user.pool.clientId,
            tokenId: this.props.user.signInUserSession.idToken.jwtToken,
            refreshToken: this.props.user.signInUserSession.refreshToken.token,
            accessToken: this.props.user.signInUserSession.accessToken.jwtToken
          }
        }
      }
      this.props.post(postData).then(response => {
        if (response['data']) {
          this.setState({ modal: 'answer' })
          this.setState({ table: undefined })
          this.setState({ segment: undefined })
          this.setState({ dimension: undefined })
          this.setState({ value: undefined })
          this.setState({ response: 'Le mappage a été ajoutée correctement !' })
        }
        else if (response['Error']) {
          this.setState({ response: `Error : ${response['Error']}` })
        }
      })
        .catch(error => reject(error))
    }
  }

  handleAuthStateChange(nextAuthState, data) {
    if (nextAuthState === 'signedout') {
      this.props.logout(data).then(_ => this.props.history.push('/login'))
    }
  }

  loadData() {
    var postData = {
      path: 'getValidSegments',
      data: {
        user: {
          email: this.props.user.attributes.email,
          poolId: this.props.user.pool.userPoolId,
          clientId: this.props.user.pool.clientId,
          tokenId: this.props.user.signInUserSession.idToken.jwtToken,
          refreshToken: this.props.user.signInUserSession.refreshToken.token,
          accessToken: this.props.user.signInUserSession.accessToken.jwtToken
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.props.post(postData).then(response => {
        if (response['data'])
          resolve(response['data'])
        else if (response['Error'])
          reject(response['Error'])
      })
        .catch(error => reject(error))
    })
  }

  loadDimensions() {
    var postData = {
      path: 'getValidDimensions',
      data: {
        table: this.state.table,
        user: {
          email: this.props.user.attributes.email,
          poolId: this.props.user.pool.userPoolId,
          clientId: this.props.user.pool.clientId,
          tokenId: this.props.user.signInUserSession.idToken.jwtToken,
          refreshToken: this.props.user.signInUserSession.refreshToken.token,
          accessToken: this.props.user.signInUserSession.accessToken.jwtToken
        }
      }
    }
    return new Promise((resolve, reject) => {
      this.props.post(postData).then(response => {
        if (response['data'])
          resolve(response['data'])
        else if (response['Error'])
          reject(response['Error'])
      })
        .catch(error => reject(error))
    })
  }

  componentDidMount() {
    if (!this.props.user || !this.props.user.attributes || !this.props.user.attributes.email)
      this.props.history.push('/login');
    else
      this.loadData().then(data => {
        this.setState({ segments: data['segments'] })
        this.setState({ tables: data['tables'] })
      })
        .catch(error => this.setState({ 'error': error }))
  }

  componentDidUpdate(_, prevState) {
    if (this.state.table && (prevState.table !== this.state.table)) {
      this.loadDimensions().then(data => {
        this.setState({ dimensions: data })
      })
        .catch(error => this.setState({ 'error': error }))
    }
  }

  render() {
      if (this.state.segments && this.state.tables) 
      return (
        <div id="wrapper" className="container center-align">
          <h1>Ajout de mappings entre les segments et le contenu</h1>
          <br />
          <label className="form"><p>Avec ce formulaire, vous pouvez ajouter des mappages entre dimensions des tables egos et les segments d'affinité.</p></label>
          <br />
          <form>
            <div className="row">
              <div className="col m5"></div>
              <div className="col m2">
                <br />
                <div className="row left-align">
                  <label className="form">
                    <p>1. Choisissez une table :</p><br />
                    <select style={{ 'display': 'inline', 'width': '100%' }} value={this.state.table} onChange={this.handleTableChange}>
                      <option value={undefined}></option>
                      {this.state.tables.map(d => <option key={d} value={d}>{d}</option>)}
                    </select>
                  </label>
                </div>
                <br />
                <div className="row left-align">
                  <label className="form">
                    <p>2. Choisissez une dimension :</p><br />
                    <select style={{ 'display': 'inline', 'width': '100%' }} value={this.state.dimension} onChange={this.handleDimensionChange}>
                      <option value={undefined}></option>
                      {this.state.dimensions.map(d => <option key={d} value={d}>{d}</option>)}
                    </select>
                  </label>
                </div>
                <br />
                <div className="row left-align">
                  <label className="form">
                    <p>4. Choisissez un segment à attribuer :</p><br />
                    <select style={{ 'display': 'inline', 'width': '100%' }} value={this.state.segment} onChange={this.handleSegmentChange}>
                      <option value={undefined}></option>
                      {this.state.segments.map(d => <option key={d} value={d}>{d}</option>)}
                    </select>
                  </label>
                </div>
                <br />
                <div className="row left-align">
                  <label className="form"><p>3. Correspondance :</p><br />
                    <input type="text" value={this.state.value} onChange={this.handleValueChange} />
                  </label>
                </div>
                <br />
                <br />
                <Modal
                  trigger={trigger}
                  actions={[
                    <Button flat modal="close" node="button" waves="black">Fermer</Button>,
                    (this.state.modal === 'form') ?<a onClick={this.handleSubmit} className="waves-effect btn black white-text">Confirmer</a>:''
                  ]}
                  className="center-align" header="Vérifiez les données à ajouter">
                  {getModal(this.state)}
                </Modal>
              </div>
            </div>
            <div className="col m5"></div>
          </form>
        </div>
      )
    else if (this.state.error)
      return (
        <div id="wrapper" className="container center-align">
          <h3>{this.state.error === 'User not allowed to request method.' ? 'Vous n\'êtes pas autorisé à voir cette section. Si vous souhaitez y accéder, veuillez envoyer un courriel à <a href="mailto:gestion.donnes@rtbf.be">gestion.donnes@rtbf.be</a>.' : this.state.error}</h3>
        </div>
      )
    else return ((
      <div id="wrapper" className="container center-align">
        <br /><br />
        <Loader></Loader>
      </div>))
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    post: (params) => dispatch(post(params))
  };
};

UpdateSegments = connect(mapStateToProps, mapDispatchToProps)(UpdateSegments);

export default withRouter(UpdateSegments);
