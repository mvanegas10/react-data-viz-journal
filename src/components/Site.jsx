import React, { Component, Fragment } from "react";
import * as d3 from 'd3'

class Site extends Component {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (this.props.title ? (
            <div className="row" style={{'cursor':'pointer'}} onClick={this.props.onClick}>
                <svg width={document.body.clientWidth * 0.9 > 800? 800: document.body.clientWidth * 0.9} height="80">
                    <rect x="0" y="0" width="80" height="80" fill={this.props.color}></rect>
                    <text x="40" y="55" style={{'fontSize':'50px', 'textAnchor': 'middle', 'fill': d3.color(this.props.color).darker()}} >{this.props.title.substring(0,1)}</text>
                    <text x="100" y="50" style={{'fontSize':'22px'}} >{this.props.title}</text>
                </svg>
            </div>
    ) : <></>
    )
  }
}

export default Site;
