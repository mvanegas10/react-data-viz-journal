import React, { Component, Fragment } from "react";

class Author extends Component {
  constructor(props) {
    super(props);
    // style={{'maxWidth': this.props.width > 400? 400: this.props.width}}
  }

  render() {
    return (this.props.width) ?(      
      <div style={{'textAlign': 'center'}}>
        <div style={{'display': 'inline-block', 'width': this.props.width > 400? 400: this.props.width}}>
          <ul style={{'border': 'white'}} className="collection center-align">
          <a href={this.props.link} target='_blank'><li className="collection-item avatar">
              <img src={this.props.icon}  alt="" className="circle"></img>
              <span className="title teal-text lighten-2" style={{'verticalAlign': 'middle', 'textAlign': 'left', 'display': 'block', 'lineHeight': '50px'}}>{this.props.name} <span style={{'fontWeight': 'normal'}} className="black-text">{this.props.date}</span></span>              
            </li>
            </a>
          </ul>
        </div>
      </div>      
    ) : <></>
  }
}

export default Author;
