import React, { Component, Fragment } from "react";
import * as d3 from 'd3'

class Swatches extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'scale': d3.scaleOrdinal()
      .domain(this.props.text)
      .range(this.props.colors)
    }
  }

  render() {
    return (typeof this.state.scale === typeof (_ => _)) ?(
      <svg width={this.props.text.length*100} height="50">{this.props.text.map((d, i) => (<g key={`g.colorlabel${i}`}><rect key={`rect.colorlabel${i}`} x={i*100} y="0" width="20" height="20" fill={`${this.state.scale(d)}`}></rect><text key={`text.colorlabel${i}`} style={{'fontSize': '11px'}} x={i*100 + 25} y="15">{`${d}`}</text></g>))}</svg>
    ) : <></>
  }
}

export default Swatches;
