import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { AmplifySignOut } from '@aws-amplify/ui-react';
import { logout } from '../actions/authenticationAction'

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.handleAuthStateChange = this.handleAuthStateChange.bind(this)
  }

  handleAuthStateChange(nextAuthState, data) {
    if (nextAuthState === 'signedout') {
      this.props.logout(data).then(_ => this.props.history.push('/login'))
    }
  }

  render() {
    return this.props.user && this.props.user.username && this.props.user.attributes.email ? (
      <div className="navbar-fixed">
        <nav>
          <div className="nav-wrapper">
            <a
              onClick={(_) => {
                this.props.history.push("/home");
              }}
              className="brand-logo"
            >
              <img
                src={process.env.PUBLIC_URL + "/img/LOGO_RTBF.BE_BL.png"}
              ></img></a>
            <span>Hello, {this.props.user.attributes.email}</span>
            <AmplifySignOut 
              buttonText="Se déconnecter"
              handleAuthStateChange={this.handleAuthStateChange} />
          </div>
        </nav>
      </div>
    ) : (
        <div className="navbar-fixed">
          <nav>
            <div className="nav-wrapper">
              <a
                onClick={(_) => {
                  this.props.history.push("/home");
                }}
                className="brand-logo"
              >
                <img
                  src={process.env.PUBLIC_URL + "/img/LOGO_RTBF.BE_BL.png"}
                ></img>
              </a>
            </div>
          </nav>
        </div>
      );
  }
}

const mapStateToProps = (store) => {
  return {
    user: store.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (params) => dispatch(logout(params))
  };
};

NavBar = connect(mapStateToProps, mapDispatchToProps)(NavBar);

export default withRouter(NavBar);
