import React, { Component, Fragment } from "react";

class Loader extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only" style={{borderColor: 'black'}}>
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    )
  }
}

export default Loader;
