export function post(params) {
    return async dispatch => {
        return new Promise((resolve, reject) => fetch(`${process.env.REACT_APP_API}${params.path}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params.data)
        }).then(response =>
            resolve(response.json())    
        ))
    }
}