export function login(data) {
    return async dispatch => {
        return new Promise((resolve, reject) => {
            try {
                if (data && data.username) {
                    dispatch({
                        type: 'LOGIN',
                        payload: data
                    })
                    resolve(data)   
                }
            } catch (error) {
                console.log('error signing up:', error);
                reject(error)
            }
        })
    }
}

export function logout() {
    return async dispatch => {
        return new Promise((resolve, reject) => {
            try {
                dispatch({
                    type: 'LOGOUT'
                })
                resolve(true)   
            } catch (error) {
                console.log('error signing out:', error);
                reject(error)
            }
        })
    }
}