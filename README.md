<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Environmental variables](#environmental-variables)
* [Running the app](#running-the-app) 
  * [Deployment version](#deployment-version) 
  * [Production build](#production-build)
* [Contributing](#contributing)   
* [Contributors](#contributors)

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

In order to deploy the application, you need to install the prerequisites listen in the BackEnd/requirements.txt and FrontEnd/package.json file. Do the following:
* npm (>= 0.0.0 required)
* react (>=17.0.1 required)

### Installation

1. Clone the repo
```sh
git clone git@gitlab.com:mvanegas10/react-data-viz-journal.git
```
2. Install NPM packages
```sh
npm install
```

### Environmental variables

Modify the file `.env` at the project's root to indicate the back-end hostname:
```
REACT_APP_API=https://viz.bda.rtbf.be/
```

## Running the app
### Deployment version
In the project's root, execute:
```sh
npm start
```
Your app should be up and running at [http://localhost:3000](http://localhost:3000)!
## Production build
In the project's root, execute:
```sh
npm run build
```
The minified production build files should appear at the `/build` folder.
<!-- CONTACT -->

## Contributing

### Creating a new view
To create a new view, you have to follow the next steps.
1. Create the view and save it at `src/views` using the react extention `.jsx` 
2. Update the router to consider your new path at `src/routes/index.js`. **Tip:** Add a new object in the `routes` array following the same structure as the existing ones and don't forget to import the view at the beging of the file!
3. Update the landing page to consider the new path at `src/views/Home.jsx`. **Tip:** Add a new object in the `sites` array following the same structure as the existing ones and set the same path as the one declared in the step!

## Contributors
| [![Meili Vanegas-Hernandez](https://avatars.githubusercontent.com/mvanegas10?s=100)<br /><sub>Meili Vanegas-Hernandez</sub>](https://mvanegas10.github.io/)<br /> |
| :---: |
