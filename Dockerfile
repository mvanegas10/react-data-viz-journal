FROM nginx:stable-alpine
COPY build /usr/share/nginx/html/journal
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]